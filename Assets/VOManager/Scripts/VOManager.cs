﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// VOManager
/// Description: Manages and handles how the clips from the VOBank are played.
/// </summary>

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(VOBank))]
public class VOManager : MonoBehaviour
{
    // global variables
    public static VOManager Instance;   // instance to access our script globally
    public Text uiTextObject;  // get a reference to our ui text object

    // private variables
    private VOBank m_voBank;  // our vo bank reference
    private AudioSource m_audSrc;   // get a reference to our default audio source
    private AudioSource m_externalAudSrc;   // get a reference to our external audio source if there is any
    private bool m_blnAudioTriggered;   // check if our audio is playing or not
    private bool m_blnSubtitleTriggered;    // check if our subtitle is showing
    private bool m_blnSetSubtitle;  // check if our subtitle is set
    private int m_intSubtitleIndex; // keep track of the current subtitle index
    private int m_intSubtitleCount; // keep track of the subtitle count
    private float m_fltSubtitleTimer;   // keep track of how long a subtitle should be displayed
    private List<SubtitleProperties> m_subtitleList;    // keep reference of the current subtitle list

    // on loaded
    void Awake()
    {
        Instance = this;    // set the instance to this object reference
        m_voBank = this.GetComponent<VOBank>();   // get the vo bank reference
        m_audSrc = this.GetComponent<AudioSource>();    // get the audio source on this object
        m_externalAudSrc = null;    // reset the external audio source
        m_subtitleList = new List<SubtitleProperties>();    // create a new list
    }

    // on start
    private void Start()
    {
        try
        {
            uiTextObject.text = "";   // reset the ui text object
        }
        catch
        {
            Debug.LogError("No UI Text Object has been set.");
        }

        m_audSrc.clip = null;   // empty the current clip
        m_audSrc.loop = false;  // turn off looping
        m_audSrc.playOnAwake = false;   // disable play on awake
        m_blnAudioTriggered = false;    // set audio trigger to false
        m_blnSetSubtitle = false;   // set subtitle to false
        m_intSubtitleIndex = 0; // reset the Subtitle index
    }

    // on each frame
    private void Update()
    {
        // if the audio is triggered
        if (m_blnAudioTriggered)
        {
            // if there isn't an external source
            if (m_externalAudSrc == null)
            {
                // if the audio is not playing 
                if (!m_audSrc.isPlaying)
                {
                    // reset the audio source
                    m_audSrc.clip = null;

                    // reset the trigger
                    m_blnAudioTriggered = false;
                }
            }
            else
            {
                // if the external audio is not playing 
                if (!m_externalAudSrc.isPlaying)
                {
                    // clear the audio source
                    m_externalAudSrc.clip = null;
                    m_externalAudSrc = null;

                    // reset the trigger
                    m_blnAudioTriggered = false;
                }
            }
        }

        // if the subtitle is triggered
        if (m_blnSubtitleTriggered)
        {

            // if the subtitle is being set
            if (m_blnSetSubtitle)
            {
                // set the subtitle 
                uiTextObject.text = m_subtitleList[m_intSubtitleIndex].subtitle;
                m_fltSubtitleTimer = m_subtitleList[m_intSubtitleIndex].duration;

                // turn off the set subtitle trigger
                m_blnSetSubtitle = false;
            }
            else
            {
                // if the timer minus subtitle fade speed is greater than 0
                if (m_fltSubtitleTimer > 0)
                {
                    // begin the countdown
                    m_fltSubtitleTimer -= Time.deltaTime;
                }
                else
                {
                    // if the index does not equal the subtitle count and the audio source is playing
                    if (m_intSubtitleIndex < m_intSubtitleCount)
                    {
                        // increment the index
                        m_intSubtitleIndex++;

                        // start fading in
                        m_blnSetSubtitle = true;
                    }
                    else
                    {
                        // empty out the subtitle
                        uiTextObject.text = "";

                        // reset the subtitle trigger
                        m_blnSubtitleTriggered = false;
                    }
                }
            }
        }
    }

    // find and return an audio with the corresponding id
    private ClipProperties FindAudioByID(int _id)
    {
        // sort through the list
        foreach (ClipProperties clip in m_voBank.bank)
        {
            // find the matching id
            if (clip.id == _id)
                // return the matching clip
                return clip;
        }

        // if nothing return null
        return null;
    }

    // find and return an audio with the corresponding name
    private ClipProperties FindAudioByName(string _name)
    {
        // sort through the list
        foreach (ClipProperties clip in m_voBank.bank)
        {
            // find the matching id
            if (clip.strName == _name)
                // return the matching clip
                return clip;
        }

        // if nothing return null
        return null;
    }

    // stop audio sources from playing and reset sources
    private void StopAndResetAudSrc()
    {
        // if there is an external audio source
        if (m_externalAudSrc)
        {
            // stop the enteral audio source and remove the clip
            m_externalAudSrc.Stop();
            m_externalAudSrc.clip = null;
            m_externalAudSrc = null;
        }
        else
        {
            // stop audio source and remove the clip
            m_audSrc.Stop();
            m_audSrc.clip = null;
        }
    }

    // display the subtitle
    private void PlaySubtitle(List<SubtitleProperties> _subtitleList)
    {
        m_intSubtitleIndex = 0;   // set the subtitle index
        m_fltSubtitleTimer = 0; // reset the subtitle timer
        m_intSubtitleCount = _subtitleList.Count - 1;   // set thesubtitle count
        m_blnSubtitleTriggered = true;  // trigger the subtitle
        m_blnSetSubtitle = true;    // set the subtitle
        m_subtitleList = _subtitleList; // set the subtitle list
        uiTextObject.text = ""; // reset the subtitle
    }

    // play the audio file through the audio source with the correct settings
    private void Play(bool _usingID, int _id, string _name, bool _usingExternalSrc, AudioSource _audSrc)
    {
        // if an audio clip is already playing
        if (m_blnAudioTriggered)
            // return and don't run anything else
            return;
        // else
        else
        {
            // try and run the following
            try
            {
                // create a new clip reference 
                ClipProperties clip = new ClipProperties();

                // if we're using an id
                if (_usingID)
                    // find and return the current audio we are playing
                    clip = FindAudioByID(_id);
                else
                    // find and return the current audio we are playing
                    clip = FindAudioByName(_name);

                // if we're using an external src
                if (_usingExternalSrc)
                {
                    // set the source and play the audio
                    m_externalAudSrc = _audSrc;
                    m_externalAudSrc.clip = clip.audClp;
                    m_externalAudSrc.Play();
                }
                else
                {
                    // set the source and play the audio
                    m_audSrc.clip = clip.audClp;
                    m_audSrc.Play();
                }

                m_blnAudioTriggered = true;

                // play the subtitle
                PlaySubtitle(clip.subtitle);
            }
            catch
            {
                // throw a warning
                Debug.LogError("There has been an error playing your subtitle. Please double check and make sure the VO Bank and Manager is configured correctly.");
            }
        }
    }

    // play an audio through the default source via an id
    public void Play(int _id)
    {
        // play the audio file through the audio source with the correct settings
        Play(true, _id, null, false, null);
    }

    // play an audio to a source via an id
    public void Play(AudioSource _audSrc, int _id)
    {
        // play the audio file through the audio source with the correct settings
        Play(true, _id, null, true, _audSrc);
    }

    // play an audio through the default source via a name
    public void Play(string _name)
    {
        // play the audio file through the audio source with the correct settings
        Play(false, 0, _name, false, null);
    }

    // play an audio to a source via a name
    public void Play(AudioSource _audSrc, string _name)
    {
        // play the audio file through the audio source with the correct settings
        Play(false, 0, _name, true, _audSrc);
    }

    // force play the audio file through the audio source with the correct settings
    private void ForcePlay(bool _usingID, int _id, string _name, bool _usingExternalSrc, AudioSource _audSrc)
    {
        // force stop the audio
        Stop();

        // try and run the following
        try
        {
            // create a new clip reference 
            ClipProperties clip = new ClipProperties();

            // if we're using an id
            if (_usingID)
                // find and return the current audio we are playing
                clip = FindAudioByID(_id);
            else
                // find and return the current audio we are playing
                clip = FindAudioByName(_name);

            // if we're using an external src
            if (_usingExternalSrc)
            {
                // set the source and play the audio
                m_externalAudSrc = _audSrc;
                m_externalAudSrc.clip = clip.audClp;
                m_externalAudSrc.Play();
            }
            else
            {
                // set the source and play the audio
                m_audSrc.clip = clip.audClp;
                m_audSrc.Play();
            }

            m_blnAudioTriggered = true;

            // play the subtitle
            PlaySubtitle(clip.subtitle);
        }
        catch
        {
            // throw a warning
            Debug.LogError("There has been an error playing your subtitle. Please double check and make sure the VO Bank and Manager is configured correctly.");
        }
    }

    // force play an audio to the default source via an id
    public void ForcePlay(int _id)
    {
        // force play the audio file through the audio source with the correct settings
        ForcePlay(true, _id, null, false, null);
    }

    // force play an audio to a source via an id
    public void ForcePlay(AudioSource _audSrc, int _id)
    {
        // force play the audio file through the audio source with the correct settings
        ForcePlay(true, _id, null, true, _audSrc);
    }

    // force play an audio to the default source via a name
    public void ForcePlay(string _name)
    {
        // force play the audio file through the audio source with the correct settings
        ForcePlay(false, 0, _name, false, null);
    }

    // force play an audio to a source via a name
    public void ForcePlay(AudioSource _audSrc, string _name)
    {
        // force play the audio file through the audio source with the correct settings
        ForcePlay(false, 0, _name, true, _audSrc);
    }

    // stop an audio file from playing
    public void Stop()
    {
        // stop audio sources from playing and reset sources
        StopAndResetAudSrc();

        // start the fade out process
        m_fltSubtitleTimer = 0;
        m_intSubtitleIndex = m_intSubtitleCount;
    }

    // check if the audio source is playing
    public bool IsPlaying()
    {
        // if there is an external audio source
        if (m_externalAudSrc)
        {
            // return if the audio source is playing or not
            return m_externalAudSrc.isPlaying;
        }
        else
        {
            // return if the audio source is playing or not
            return m_audSrc.isPlaying;
        }
    }
}