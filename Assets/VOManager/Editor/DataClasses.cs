﻿using UnityEngine;
using System.Collections.Generic;

// contains information for each audio set
[System.Serializable]
public class AudioData
{
    public string uniqueID; // hold a unique id for each audio data entry
    public AudioClip audioClip; // the audio clip
    public bool includeInVOLibrary;    // check if the audio should be included in the vo library
    public int id;  // the current id
    public double length;   // the audio file length
    public string uniqueName;   // the audio's unique name
    public string fileName; // the file name
    public string extension;    // the file extension
    public string path; // the file path
    public string filePath; // the full pathway with file name and extention
    public bool isMissing;  // check if the audio file is missing
    public List<SubtitleData> subtitle; // holds the subtitle information
}

// contains subtitle informatino for each audio set
[System.Serializable]
public class SubtitleData
{
    public string subtitle; // the text to be displayed
    public float duration;  // the duration the text should be displayed for
    public bool selected;   // check if this current subtitle is selected
}
