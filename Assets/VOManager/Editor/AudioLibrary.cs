﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;

/// <summary>
/// Audio Library - Main
/// Description: Contains a list and reference to all audio files throughout the Unity project.
/// </summary>

public partial class AudioLibrary : EditorWindow
{
    #region Variables
    // reference this window
    public static AudioLibrary Instance { get; private set; }
    
    // check if this window is open or not
    public static bool IsOpen
    {
        get { return Instance != null; }
    }

    // create a list of audio data that can be referenced as the audio library
    public static List<AudioData> audioLibrary = new List<AudioData>();

    // create a list of audio data that referenced as the vo library
    public static List<AudioData> voLibrary = new List<AudioData>();

    // create an enum and string to hold our search filters and information
    public enum SearchBy { ID, FileName, Path }
    public SearchBy searchBy = SearchBy.FileName;
    public static string searchText = "";

    // create an enum to display the order of our information
    public enum OrderBy { ID, VOLibrary, Length, Extension, FileName, Path }
    public static OrderBy orderBy = OrderBy.ID;

    // create an enum to sort the order of which our informaiton is displayed
    public enum SortBy { ASC, DESC }
    public static SortBy sortBy = SortBy.ASC;

    // create a list of supported audio files
    private static string supportedAudioExt = "*.mp3,*.ogg,*.wav,*.aiff,*.aif,*.mod,*.it,*.s3m,*.xm";

    // create a reference variable to store our current row position
    private int row = 0;

    // reference scroll values for our ui
    private Vector2 m_vecScrollPos;
    private Vector2 m_vecScrollPosAudioHeaderLabel;
    private Vector2 m_vecScrollPosAudioList;

    // contains information for the popup windows
    private static CleanAudioLibraryPopup cleanAudioLibraryPopup;
    private static RegenerateLibraryIDsPopup regenerateLibraryIDsPopup;
    private static ExcludeAllPopup excludeAllPopup;

    // add the "Audio Library" option under the VO Manager context menu
    [MenuItem("VO Manager/Audio Library")]
    #endregion

    // on initialization
    private static void Init()
    {
        // get existing open window or if none, make a new one
        AudioLibrary window = (AudioLibrary)EditorWindow.GetWindow(typeof(AudioLibrary), false, "Audio Library");
        window.Show(); 
    }

    // on enabled
    private void OnEnable()
    {
        // set this instancec
        Instance = this;

        // load the audio library
        LoadAudioLibrary();
    }

    // when the window gets updated
    private void OnInspectorUpdate()
    {
        // repaint the editor window
        Repaint();
    }

    // load the audio library
    public static void LoadAudioLibrary()
    {
        // get our audio library data
        TextAsset audioLibraryData = Resources.Load("audioLibraryData") as TextAsset;

        // check if our library data file does not exists
        if(audioLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            } 

            // create our file if it doesn't exist and refresh the asset database
            StreamWriter writer = new StreamWriter(Application.dataPath + @"/Resources/audioLibraryData.json");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();

            // break out of the function
            return;
        } else
        {
            // clear out the current audio library list
            audioLibrary = new List<AudioData>();
        }
 
        // get our audio library data and store them into lists
        List<string> audioData = Regex.Matches(audioLibraryData.text, @"\[\[(.+?)\]\]").Cast<Match>().Select(m => m.Groups[1].Value).ToList();

        // for each audio set in audio data
        foreach (string audioSet in audioData)
        {
            // create a temporary variable to store our audio data
            AudioData tempAudioData = new AudioData();
            tempAudioData = JsonUtility.FromJson<AudioData>(audioSet);

            // check if the file exists or not
            tempAudioData.isMissing = CheckForMissingFile(tempAudioData);

            // add the audio data to the audio library
            audioLibrary.Add(tempAudioData);
        }

        // save the audio library
        SaveAudioLibrary();

        // load the vo library
        LoadVOLibrary();
    }

    // load the vo library
    private static void LoadVOLibrary()
    {
        // get our vo library data
        TextAsset voLibraryData = Resources.Load("voLibraryData") as TextAsset;

        // check if our library data file does not exists
        if (voLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            StreamWriter writer = new StreamWriter(Application.dataPath + @"/Resources/voLibraryData.json");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh(); 

            // break out of the function
            return;
        }
        else
        {
            // clear out the current audio library list
            voLibrary = new List<AudioData>();
        }

        // get our vo library data and store them into lists
        List<string> voData = Regex.Matches(voLibraryData.text, @"\[\[(.+?)\]\]").Cast<Match>().Select(m => m.Groups[1].Value).ToList();

        // for each audio set in audio data
        foreach (string audioSet in voData)
        {
            // create a temporary variable to store our audio data
            AudioData tempAudioData = new AudioData();
            tempAudioData = JsonUtility.FromJson<AudioData>(audioSet);

            // check if the file exists or not
            tempAudioData.isMissing = CheckForMissingFile(tempAudioData);

            // add the audio data to the vo library
            voLibrary.Add(tempAudioData);
        }

        // save the vo library
        SaveVOLibrary();
    }

    // search the asset folder for audio files
    private static bool SearchForAudioFiles()
    {
        // create a new directory info that is set to the current asset folder path
        DirectoryInfo assetDirectory = new DirectoryInfo(Application.dataPath);

        // for each file inside the asset folder where it matches our audio extensions
        foreach (FileInfo audioFile in assetDirectory.GetFiles("*.*", SearchOption.AllDirectories).Where(x => supportedAudioExt.Contains(Path.GetExtension(x.ToString()).ToLower())))
        {
            // get the current file path with file name and extension
            string fullFilePath = audioFile.FullName;
            fullFilePath = fullFilePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");

            // get the current file path without the file name and extension
            string filePath = fullFilePath;
            int index = filePath.LastIndexOf("/");
            if (index > 0)
                filePath = filePath.Substring(0, index);

            // get the audio clip
            AudioClip tempClip = AssetDatabase.LoadAssetAtPath(fullFilePath, typeof(AudioClip)) as AudioClip;

            // create a temporary variable to store our audio data
            AudioData tempAudioData = new AudioData();
            tempAudioData.audioClip = tempClip;
            tempAudioData.includeInVOLibrary = false;

            // if there are no clips in the audio library
            if (audioLibrary.Count == 0)
                // set the id to zero
                tempAudioData.id = 0;
            else
                // else set the number to the last item's id plus one
                tempAudioData.id = audioLibrary[audioLibrary.Count - 1].id + 1;

            // create and store our temporary data
            tempAudioData.length = tempClip.length;
            tempAudioData.uniqueName = tempClip.name;
            tempAudioData.fileName = tempClip.name;
            tempAudioData.extension = audioFile.Extension;
            tempAudioData.path = filePath;
            tempAudioData.filePath = filePath + "/" + tempClip.name + audioFile.Extension;
            tempAudioData.isMissing = false;
            tempAudioData.subtitle = new List<SubtitleData>();

            // assign a unique id
            Guid guid = Guid.NewGuid();
            tempAudioData.uniqueID = guid.ToString();

            // check if the audio library already contains the audio data
            if (!audioLibrary.Any(x => x.filePath == tempAudioData.filePath))
                // add the temp audio data to the audio library
                audioLibrary.Add(tempAudioData);
        }

        // save the audio library
        SaveAudioLibrary();

        // return true
        return true;
    }

    // check if the file exists or not
    private static bool CheckForMissingFile(AudioData audioData)
    {
        // get the current asset directory
        DirectoryInfo assetDirectory = new DirectoryInfo(Application.dataPath);

        // get the current file and path
        string innerPath = audioData.path.Replace("Assets", "");
        string filePath = assetDirectory + innerPath + "/" + audioData.fileName + audioData.extension;

        // check if the file exists and return true or false
        if (File.Exists(filePath))
            return false;
        else
            return true;
    }

    // save the audio library
    private static void SaveAudioLibrary()
    {
        // create a new empty string to store our data
        string audioData = "";

        // for each audio data in the audio library
        foreach (AudioData audioClip in audioLibrary)
        {
            // assign it's json value to the string
            audioData += "[[";
            audioData += JsonUtility.ToJson(audioClip);
            audioData += "]]";
        }

        // get our audio library data
        TextAsset audioLibraryData = Resources.Load("audioLibraryData") as TextAsset;

        // create a stream writer
        StreamWriter writer;

        // check if our library data file does not exists
        if (audioLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            writer = new StreamWriter(Application.dataPath + @"/Resources/audioLibraryData.json");
        } else
        {
            // else if the file exists
            writer = new StreamWriter(AssetDatabase.GetAssetPath(audioLibraryData));
        }

        // write our audio data into our audio library data files
        // and refresh the asset database
        writer.Write(audioData);
        writer.Close();
        UnityEditor.AssetDatabase.Refresh();
    }

    // save the vo library
    private static void SaveVOLibrary()
    {
        // create a new empty string to store our data
        string audioData = "";

        // for each audio data in the vo library
        foreach (AudioData audioClip in voLibrary)
        {
            // assign it's json value to the string
            audioData += "[[";
            audioData += JsonUtility.ToJson(audioClip);
            audioData += "]]";
        }

        // get our vo library data
        TextAsset voLibraryData = Resources.Load("voLibraryData") as TextAsset;

        // create a stream writer
        StreamWriter writer;

        // check if our library data file does not exists
        if (voLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            writer = new StreamWriter(Application.dataPath + @"/Resources/voLibraryData.json");
        } else
        {
            // else if the file exists
            writer = new StreamWriter(AssetDatabase.GetAssetPath(voLibraryData));
        }

        // write our audio data into our vo library data files
        // and refresh the asset database
        writer.Write(audioData);
        writer.Close();
        UnityEditor.AssetDatabase.Refresh();
    }

    // check if we should add or remove a clip from the vo library
    private static void CheckVOLibrary(AudioData audioData, bool add)
    {
        // if we are adding an audio clip
        if (add)
        {
            // perform a check to see if the audio clip exists in the library
            if (!voLibrary.Any(t=>t.uniqueID == audioData.uniqueID))
            {
                // add it to the library
                voLibrary.Add(audioData);

                // save the vo library  
                SaveVOLibrary();
            }
        }
        else
        {
            // perform a check to see if the audio clip exists in the library
            if (voLibrary.Any(t => t.uniqueID == audioData.uniqueID))
            {
                // remove it from the library
                AudioData data = voLibrary.First(t => t.uniqueID == audioData.uniqueID);
                voLibrary.Remove(data);

                // save the vo library  
                SaveVOLibrary();
            }
        }
    }

    // clear audio library and saved data
    public static void ClearAllReferences()
    {
        // get our audio library data
        TextAsset audioLibraryData = Resources.Load("audioLibraryData") as TextAsset;

        // check if our library data file does not exists
        if (audioLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            StreamWriter writer = new StreamWriter(Application.dataPath + @"/Resources/audioLibraryData.json");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();
        } else
        {
            // get the file and empty it's content
            StreamWriter writer = new StreamWriter(AssetDatabase.GetAssetPath(audioLibraryData));
            writer.Write("");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();
        }

        // clear the audio library
        audioLibrary = new List<AudioData>();

        // clear the vo library
        VOLibrary.ClearAllReferences();
    }

    // open vo library window
    private static void OpenVOLibrary()
    {
        // get existing open vo library window or if none, make a new one
        VOLibrary window = (VOLibrary)EditorWindow.GetWindow(typeof(VOLibrary), false, "VO Library");
        window.EndWindows();
    }

    // confirm if audio library ids should be regenerated
    private static void RegenerateLibraryIDsPopup()
    {
        // get existing open window or if none, make a new one
        regenerateLibraryIDsPopup = (RegenerateLibraryIDsPopup)EditorWindow.GetWindow(typeof(RegenerateLibraryIDsPopup), true, "Regenerate IDs?");
        regenerateLibraryIDsPopup.maxSize = new Vector2(500f, 90f);
        regenerateLibraryIDsPopup.minSize = regenerateLibraryIDsPopup.maxSize;
        EditorExtensions.GetWindowAndCenterOnMain(regenerateLibraryIDsPopup);
        regenerateLibraryIDsPopup.Show();

        // close all other popup windows if they exist
        if (cleanAudioLibraryPopup != null)
            cleanAudioLibraryPopup.Close();
    }

    // regenerate audio library ids
    public static void RegenerateLibraryIDs()
    {
        // reload the audio library to make sure it is loaded
        //LoadAudioLibrary();

        // for each audio data in the audio library
        foreach (AudioData audioData in audioLibrary.ToArray())
        {
            // set the audio data id to its index
            audioData.id = audioLibrary.IndexOf(audioData);

            // if the current audio data is in the vo library
            if (voLibrary.Any(x => x.uniqueID == audioData.uniqueID))
            {
                // select that item
                AudioData voData = voLibrary.First(x => x.uniqueID == audioData.uniqueID);

                // assign the new id
                voData.id = audioLibrary.IndexOf(audioData); 
            }
        }

        // save the libraries
        //SaveAudioLibrary();
        //SaveVOLibrary();

        // reload the vo library if it is open
        if (VOLibrary.IsOpen)
            VOLibrary.LoadVOLibrary();
    }

    // confirm if all the selected audio files should be excluded from the vo library
    private static void ExcludeAllSelectedFilesPopup()
    {
        // get existing open window or if none, make a new one
        excludeAllPopup = (ExcludeAllPopup)EditorWindow.GetWindow(typeof(ExcludeAllPopup), true, "Exclude All Selected Files?");
        excludeAllPopup.maxSize = new Vector2(500f, 90f);
        excludeAllPopup.minSize = excludeAllPopup.maxSize;
        EditorExtensions.GetWindowAndCenterOnMain(excludeAllPopup);
        excludeAllPopup.Show();

        // close all other popup windows if they exist
        if (cleanAudioLibraryPopup != null)
            cleanAudioLibraryPopup.Close();

        if (regenerateLibraryIDsPopup != null)
            regenerateLibraryIDsPopup.Close();
    }

    // exclude all selected files from the vo library
    public static void ExcludeAllSelectedFiles()
    {
        // for each audio data in the audio library
        foreach (AudioData audioData in audioLibrary)
        {
            // remove this from the vo library
            audioData.includeInVOLibrary = false;
            CheckVOLibrary(audioData, audioData.includeInVOLibrary);
        }

        // save the audio library
       // SaveAudioLibrary();

        // reload the vo library if it is open
        if (VOLibrary.IsOpen)
            VOLibrary.LoadVOLibrary();
    }

    // confirm if the audio library should remove all missing instances
    private static void CleanAudioLibraryPopup()
    {
        // get existing open window or if none, make a new one
        cleanAudioLibraryPopup = (CleanAudioLibraryPopup)EditorWindow.GetWindow(typeof(CleanAudioLibraryPopup), true, "Remove Missing Files?");
        cleanAudioLibraryPopup.maxSize = new Vector2(500f, 90f);
        cleanAudioLibraryPopup.minSize = cleanAudioLibraryPopup.maxSize;
        EditorExtensions.GetWindowAndCenterOnMain(cleanAudioLibraryPopup);
        cleanAudioLibraryPopup.Show();

        // close all other popup windows if they exist
        if (regenerateLibraryIDsPopup != null)
            regenerateLibraryIDsPopup.Close();
    }

    // remove all missing audio references from the audio library
    public static void CleanAudioLibrary()
    {
        // reload the audio library to make sure it is loaded
        //LoadAudioLibrary();

        // for each audio data in the audio library
        foreach (AudioData audioData in audioLibrary.ToArray())
        {
            // if the file is missing
            if (audioData.isMissing)
            {
                // remove the reference from the library
                audioLibrary.Remove(audioData);

                // if the current audio data is in the vo library
                if (voLibrary.Any(x => x.uniqueID == audioData.uniqueID))
                {
                    // select that item
                    AudioData voData = voLibrary.First(x => x.uniqueID == audioData.uniqueID);

                    // remove the item
                    CheckVOLibrary(voData, false);
                }
            }
        }

        // save the audio library
       // SaveAudioLibrary();

        // if the vo library is open 
        if (VOLibrary.IsOpen)
            // regenerate the list
            VOLibrary.LoadVOLibrary();
    }

    // order and sort the audio library 
    private static List<AudioData> OrderAndSortList(List<AudioData> audioLibrary)
    {
        // create a copy of our audio library
        List<AudioData> audioList = new List<AudioData>(audioLibrary);

        // order list by id
        if (orderBy == OrderBy.ID)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.id).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.id).ToList();

            return audioList;
        }
        // order list by rather or not it is included in the vo library
        else if (orderBy == OrderBy.VOLibrary)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.includeInVOLibrary).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.includeInVOLibrary).ToList();

            return audioList;
        }
        // order list by audio clip's length
        else if (orderBy == OrderBy.Length)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.length).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.length).ToList();

            return audioList;
        }
        // order list by file extension
        else if (orderBy == OrderBy.Extension)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.extension).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.extension).ToList();

            return audioList;
        }
        // order list by file name
        else if (orderBy == OrderBy.FileName)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.fileName).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.fileName).ToList();

            return audioList;
        }
        // order list by file path
        else if (orderBy == OrderBy.Path)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.path).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.path).ToList();

            return audioList;
        }

        return audioList;
    }
}