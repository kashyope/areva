﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Audio Library - Misc.
/// Description: Contains a list and reference to all audio files throughout the Unity project.
/// </summary>

// menu option to clear all editor preference data
public class ClearData : EditorWindow
{
    // add "Clear Data" option under the VO Manager context menu
    [MenuItem("VO Manager/Clear Data")]

    // on initialization
    static void Init()
    {
        // get existing open window or if none, make a new one
        ClearData window = (ClearData)EditorWindow.GetWindow(typeof(ClearData), true, "Delete Data");
        window.maxSize = new Vector2(500f, 90f);
        window.minSize = window.maxSize;
        EditorExtensions.GetWindowAndCenterOnMain(window);
        window.Show();
    }

    // clear all editor data preferences
    private static void ClearAllLibraryReferences()
    {
        // delete our editor preferences
        AudioLibrary.ClearAllReferences();

        // repaint the library windows
        AudioLibrary.LoadAudioLibrary();
        VOLibrary.LoadVOLibrary();
    }

    // draw the ui
    void OnGUI()
    {
        /// GUI STYLES

        GUIStyle styleText = new GUIStyle();
        styleText.padding = new RectOffset(10, 10, 10, 10);
        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleText.normal.textColor = EditorStyle.ConvertToColor(168, 168, 168, 1);
        styleText.alignment = TextAnchor.MiddleLeft;
        styleText.wordWrap = true;
        styleText.fontSize = 12;

        /// EDITOR

        EditorGUILayout.LabelField("Are you sure you wish to clear all data references in the Audio Library? This affects all data within the VO Library as well.", styleText);

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        // clean the audio library if this button is pressed
        if (GUILayout.Button("Proceed", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
        {
            ClearAllLibraryReferences();
            this.Close();
        }

        // close this window if this button is pressed
        if (GUILayout.Button("Cancel", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
            this.Close();

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }
}

// confirmation window to clean audio library of missing files
public class CleanAudioLibraryPopup : EditorWindow
{
    // draw the ui
    void OnGUI()
    {
        /// GUI STYLES

        GUIStyle styleText = new GUIStyle();
        styleText.padding = new RectOffset(10, 10, 10, 10);
        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleText.normal.textColor = EditorStyle.ConvertToColor(168, 168, 168, 1);
        styleText.alignment = TextAnchor.MiddleLeft;
        styleText.wordWrap = true;
        styleText.fontSize = 12;

        /// EDITOR

        EditorGUILayout.LabelField("Remove all missing files from the Audio Library? Please keep in mind this also removes any references to the VO Library.", styleText);

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        // clean the audio library if this button is pressed
        if (GUILayout.Button("Proceed", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
        {
            AudioLibrary.CleanAudioLibrary();
            this.Close();
        }

        // close this window if this button is pressed
        if (GUILayout.Button("Cancel", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
            this.Close();

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }
}

// confirmation window to regenerate all IDs in the audio library
public class RegenerateLibraryIDsPopup : EditorWindow
{
    // draw our ui
    void OnGUI()
    {
        /// GUI STYLES

        GUIStyle styleText = new GUIStyle();
        styleText.padding = new RectOffset(10, 10, 10, 10);
        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleText.normal.textColor = EditorStyle.ConvertToColor(168, 168, 168, 1);
        styleText.alignment = TextAnchor.MiddleLeft;
        styleText.wordWrap = true;
        styleText.fontSize = 12;

        /// EDITOR

        EditorGUILayout.LabelField("Are you sure you want to regenerate the IDs? Please keep in mind this will also change any references to the vo library.", styleText);

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        // regenerate the audio clip ids in the library if this button is pressed
        if (GUILayout.Button("Proceed", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
        {
            AudioLibrary.RegenerateLibraryIDs();
            this.Close();
        }

        // close this window if this button is pressed
        if (GUILayout.Button("Cancel", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
            this.Close();

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }
}

// confirmation window to exclude all audio files that are selected from the vo library
public class ExcludeAllPopup : EditorWindow
{
    // draw our ui
    void OnGUI()
    {
        /// GUI STYLES

        GUIStyle styleText = new GUIStyle();
        styleText.padding = new RectOffset(10, 10, 10, 10);
        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleText.normal.textColor = EditorStyle.ConvertToColor(168, 168, 168, 1);
        styleText.alignment = TextAnchor.MiddleLeft;
        styleText.wordWrap = true;
        styleText.fontSize = 12;

        /// EDITOR

        EditorGUILayout.LabelField("Are you sure you want to exclude all files from the VO Library? Please keep in mind this change cannot be undone.", styleText);

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        // exclude all selected files from the vo library if this button is pressed
        if (GUILayout.Button("Proceed", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
        {
            AudioLibrary.ExcludeAllSelectedFiles();
            this.Close();
        }

        // close this window if this button is pressed
        if (GUILayout.Button("Cancel", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
            this.Close();

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }
}