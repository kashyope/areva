﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// contains a list of functions for GUI editor styles
public class EditorStyle
{
    // create a texture background
    public static Texture2D SetBackground(int width, int height, Color color)
    {
        Color[] pixels = new Color[width * height];

        for (int i = 0; i < pixels.Length; i++)
            pixels[i] = color;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pixels);
        result.Apply();

        return result;
    }

    // convert the regular rgba color to unity's 0-1 
    public static Color ConvertToColor(float r, float g, float b, float a)
    {
        return new Color(r / 255.0f, g / 255.0f, b / 255.0f, a);
    }
}

/// <summary>
/// Editor Extension Class
/// Adds extra methods to enhance the basic editor.
///
/// Notes
/// The core plugin does not require this class to run.
/// This class is only meant to place editor windows
/// appropriately for convenience.
/// 
/// Credits
/// The methods in this script were posted by Bunny83
/// on Unity Answers. Some of the variables and method
/// names were renamed for convenience.
/// http://answers.unity3d.com/questions/960413/editor-window-how-to-center-a-window.html
/// </summary>

public static class EditorExtensions
{
    // get all derived types
    public static System.Type[] GetAllDerivedTypes(this System.AppDomain aAppDomain, System.Type aType)
    {
        var result = new List<System.Type>();
        var assemblies = aAppDomain.GetAssemblies();
        foreach (var assembly in assemblies)
        {
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                if (type.IsSubclassOf(aType))
                    result.Add(type);
            }
        }
        return result.ToArray();
    }

    // get the current position of the main window
    public static Rect GetMainWindowPosition()
    {
        var containerWinType = System.AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(ScriptableObject)).Where(t => t.Name == "ContainerWindow").FirstOrDefault();
        if (containerWinType == null)
            throw new System.MissingMemberException("There has been an error trying to find the interal type ContainerWindow.");
        var showModeField = containerWinType.GetField("m_ShowMode", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
        var positionProperty = containerWinType.GetProperty("position", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        if (showModeField == null || positionProperty == null)
            throw new System.MissingFieldException("There has been an error trying to find the internal fields 'm_ShowMode' and 'position.'");
        var windows = Resources.FindObjectsOfTypeAll(containerWinType);
        foreach (var win in windows)
        {
            var showmode = (int)showModeField.GetValue(win);
            if (showmode == 4)
            {
                var pos = (Rect)positionProperty.GetValue(win, null);
                return pos;
            }
        }
        throw new System.NotSupportedException("There has been an error trying to find the internal main window.");
    }

    // get the current window and center it on the screen
    public static void GetWindowAndCenterOnMain(this UnityEditor.EditorWindow window)
    {
        Rect mainWindowPos = GetMainWindowPosition();
        Rect windowPos = window.position;
        float w = (mainWindowPos.width - windowPos.width) * 0.5f;
        float h = (mainWindowPos.height - windowPos.height) * 0.5f;
        windowPos.x = mainWindowPos.x + w;
        windowPos.y = mainWindowPos.y + h;
        window.position = windowPos;
    }
}