﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.IO;

/// <summary>
/// VO Library - Main
/// Description: Contains a list and reference to all audio files included in the vo library.
/// </summary>

public partial class VOLibrary : EditorWindow
{
    #region Variables
    // reference this window
    public static VOLibrary Instance { get; private set; }

    // check if this window is open or not
    public static bool IsOpen
    {
        get { return Instance != null; }
    }

    // create a list of audio data that referenced as the vo library
    public static List<AudioData> voLibrary = new List<AudioData>();

    // create an enum and string to hold our search filters and information
    public enum SearchBy { ID, UniqueName, FilePath }
    public SearchBy searchBy = SearchBy.UniqueName;
    public static string searchText = "";

    // create an enum to display the order of our information
    public enum OrderBy { ID, Length, UniqueName, FilePath }
    public static OrderBy orderBy = OrderBy.ID;

    // create an enum to sort the order of which our informaiton is displayed
    public enum SortBy { ASC, DESC }
    public static SortBy sortBy = SortBy.ASC;

    // create a reference variable to store our current row position
    private int row = 0;

    // reference scroll values for our ui
    private Vector2 m_vecScrollPos;
    private Vector2 m_vecScrollPosAudioHeaderLabel;
    private Vector2 m_vecScrollPosAudioList;

    // add "VO Library" option under the VO Manager context menu
    [MenuItem("VO Manager/VO Library")]
    #endregion

    // on initialization
    static void Init()
    {
        // get existing open window or if none, make a new one
        VOLibrary window = (VOLibrary)EditorWindow.GetWindow(typeof(VOLibrary), false, "VO Library");
        window.Show();

        // load the vo library
        LoadVOLibrary();
    }

    // on enabled
    private void OnEnable()
    {
        // set this instance
        Instance = this;

        // load the vo library
        LoadVOLibrary();
    }

    // when the window gets updated
    private void OnInspectorUpdate()
    {
        // repaint the editor window
        Repaint();
    }

    // load the vo library
    public static void LoadVOLibrary()
    {
        // get our vo library data
        TextAsset voLibraryData = Resources.Load("voLibraryData") as TextAsset;

        // check if our library data file does not exists
        if (voLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            StreamWriter writer = new StreamWriter(Application.dataPath + @"/Resources/voLibraryData.json");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();

            // break out of the function
            return;
        }
        else
        {
            // clear out the current audio library list
            voLibrary = new List<AudioData>();
        }

        // get our vo library data and store them into lists
        List<string> voData = Regex.Matches(voLibraryData.text, @"\[\[(.+?)\]\]").Cast<Match>().Select(m => m.Groups[1].Value).ToList();

        // for each audio set in audio data
        foreach (string audioSet in voData)
        {
            // create a temporary variable to store our audio data
            AudioData tempAudioData = new AudioData();
            tempAudioData = JsonUtility.FromJson<AudioData>(audioSet);

            // check if the file exists or not
            tempAudioData.isMissing = CheckForMissingFile(tempAudioData);

            // add the audio data to the vo library
            voLibrary.Add(tempAudioData);
        }
    }

    // check if the file exists or not
    private static bool CheckForMissingFile(AudioData audioData)
    {
        // get the current asset directory
        DirectoryInfo assetDirectory = new DirectoryInfo(Application.dataPath);

        // get the current file and path
        string innerPath = audioData.path.Replace("Assets", "");
        string filePath = assetDirectory + innerPath + "/" + audioData.fileName + audioData.extension;

        // check if the file exists and return true or false
        if (File.Exists(filePath))
            return false;
        else
            return true;
    }

    // check the audio library for missing files
    private static void CheckLibraryForMissingFiles()
    {
        // for each audio data in the library
        foreach (AudioData audioData in voLibrary)
        {
            // if the file is missing
            if (CheckForMissingFile(audioData))
            {
                // mark the file as missing
                audioData.isMissing = true;
                voLibrary[audioData.id].isMissing = true;
            }
            else
            {
                audioData.isMissing = false;
                voLibrary[audioData.id].isMissing = false;
            }
        }

        // save the current library
        SaveVOLibrary();
    }

    // save the vo library
    private static void SaveVOLibrary()
    {
        // create a new empty string to store our data
        string audioData = "";

        // for each audio data in the vo library
        foreach (AudioData audioClip in voLibrary)
        {
            // assign it's json value to the string
            audioData += "[[";
            audioData += JsonUtility.ToJson(audioClip);
            audioData += "]]";
        }

        // get our vo library data
        TextAsset voLibraryData = Resources.Load("voLibraryData") as TextAsset;

        // create a stream writer
        StreamWriter writer;

        // check if our library data file does not exists
        if (voLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            writer = new StreamWriter(Application.dataPath + @"/Resources/voLibraryData.json");
        }
        else
        {
            // else if the file exists
            writer = new StreamWriter(AssetDatabase.GetAssetPath(voLibraryData));
        }

        // write our audio data into our vo library data files
        // and refresh the asset database
        writer.Write(audioData);
        writer.Close();
        UnityEditor.AssetDatabase.Refresh();
    }

    // clear vo library and saved data
    public static void ClearAllReferences()
    {
        // get our vo library data
        TextAsset voLibraryData = Resources.Load("voLibraryData") as TextAsset;

        // check if our library data file does not exists
        if (voLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            StreamWriter writer = new StreamWriter(Application.dataPath + @"/Resources/voLibraryData.json");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();
        }
        else
        {
            // get the file and empty it's content
            StreamWriter writer = new StreamWriter(AssetDatabase.GetAssetPath(voLibraryData));
            writer.Write("");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();
        }

        // clear the vo library
        voLibrary = new List<AudioData>();
    }

    // order and sort the vo library
    private static List<AudioData> OrderAndSortList(List<AudioData> audioLibrary)
    {
        // create a copy of our vo library
        List<AudioData> audioList = new List<AudioData>(audioLibrary);

        // order list by id
        if (orderBy == OrderBy.ID)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.id).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.id).ToList();

            return audioList;
        }
        // order list by audio clip's length
        else if (orderBy == OrderBy.Length)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.length).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.length).ToList();

            return audioList;
        }
        // order list by file name
        else if (orderBy == OrderBy.UniqueName)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.uniqueName).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.uniqueName).ToList();

            return audioList;
        }
        // order list by file path
        else if (orderBy == OrderBy.FilePath)
        {
            if (sortBy == SortBy.ASC)
                audioList = audioList.OrderBy(x => x.filePath).ToList();
            else
                audioList = audioList.OrderByDescending(x => x.filePath).ToList();

            return audioList;
        }

        return audioList;
    }

    // name and generate a prefab for the vo bank
    private static void GenerateVOBankPopup()
    {
        // get existing open window or if none, make a new one
        GenerateVOBankPopup window = (GenerateVOBankPopup)EditorWindow.GetWindow(typeof(GenerateVOBankPopup), true, "Generate VO Bank");
        window.maxSize = new Vector2(500f, 110f);
        window.minSize = window.maxSize;
        EditorExtensions.GetWindowAndCenterOnMain(window);
        window.Show();
    }

    // generate a vo bank prefab
    public void GenerateVOBank(string name)
    {
        // create a clip properties list
        List<ClipProperties> clipProperties = new List<ClipProperties>();

        // foreach item in the vo library
        foreach (AudioData audioData in voLibrary)
        {
            // create a new instance for clip properties
            ClipProperties clipProperty = new ClipProperties();

            // copy the information from the library
            clipProperty.id = audioData.id;

            clipProperty.audClp = (AudioClip)AssetDatabase.LoadAssetAtPath(audioData.filePath, typeof(AudioClip));
            clipProperty.strName = audioData.uniqueName;

            // foreach subtitle in the audio data
            foreach (SubtitleData subtitleData in audioData.subtitle)
            {
                // create a new instance for subtitle properties
                SubtitleProperties subtitleProperty = new SubtitleProperties();

                // copy the information from the library
                subtitleProperty.duration = subtitleData.duration;
                subtitleProperty.subtitle = subtitleData.subtitle;

                // add the subtitle to the list
                clipProperty.subtitle.Add(subtitleProperty);
            }

            // add the audio data to the list
            clipProperties.Add(clipProperty);
        }

        // if there isn't a prefabs folder in the asset folder
        if (!System.IO.Directory.Exists(Application.dataPath + "/Prefabs/"))
        {
            // create the prefab folder and refresh
            System.IO.Directory.CreateDirectory(Application.dataPath + "/Prefabs/");
            AssetDatabase.Refresh();
        }

        // save the prefab into the scene and save it into the asset prefabs folder
        GameObject voBankObject = new GameObject();
        voBankObject.AddComponent<VOBank>();
        voBankObject.GetComponent<VOBank>().bank = clipProperties;
        voBankObject.name = name;
        PrefabUtility.CreatePrefab("Assets/Prefabs/" + name + ".prefab", voBankObject, ReplacePrefabOptions.ConnectToPrefab);
        AssetDatabase.Refresh();
    }
}
