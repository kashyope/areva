﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

/// <summary>
/// Subtitle Editor
/// Description: Contains a list and reference to all subtitles information for each audio set.
/// </summary>

public class SubtitleEditor : EditorWindow
{
    // get reference to the current audio data
    private static AudioData audioData = new AudioData();

    // create a reference variable to store our current index position
    private int index = 0;

    // reference scroll values for our ui
    private Vector2 m_vecScrollPos;
    private Vector2 m_vecScrollPosSubtitleHeaderLabel;
    private Vector2 m_vecScrollPosSubtitleList;

    // contains information for the popup windows
    private static RemoveSelectedPopup removeSelectedPopup;

    // when the window gets updated
    private void OnInspectorUpdate()
    {
        // repaint the editor window
        Repaint();
    }

    // set the audio data
    public void SetAudioData(AudioData _audioData)
    {
        // if there is audio data being passed through
        if (_audioData != null)
            // set the audio data
            audioData = _audioData;
    }

    // save the subtitle settings into the vo library
    private static void LoadSubtitleToVOLibrary()
    {
        // get our vo library data
        TextAsset voLibraryData = Resources.Load("voLibraryData") as TextAsset;

        // check if our library data file does not exists
        if (voLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            StreamWriter writer = new StreamWriter(Application.dataPath + @"/Resources/voLibraryData.json");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();
        }

        // create a reference for the vo library
        List<AudioData> voLibrary = new List<AudioData>();

        // get our vo library data and store them into lists
        List<string> voData = Regex.Matches(voLibraryData.text, @"\[\[(.+?)\]\]").Cast<Match>().Select(m => m.Groups[1].Value).ToList();

        // for each audio set in audio data
        foreach (string audioSet in voData)
        {
            // create a temporary variable to store our audio data
            AudioData tempAudioData = new AudioData();
            tempAudioData = JsonUtility.FromJson<AudioData>(audioSet);

            // add the audio data to the vo library
            voLibrary.Add(tempAudioData);
        }

        // find the current audio set in the vo library and update it
        AudioData currentAudioSet = voLibrary.Where(t => t.uniqueID == audioData.uniqueID).SingleOrDefault();

        // if the audio set has subtitles
        if(audioData.subtitle != null)
            currentAudioSet.subtitle = audioData.subtitle;

        // if the audio set has a unique name
        if (audioData.uniqueName != null)
            currentAudioSet.uniqueName = audioData.uniqueName;

        // save the vo library
        SaveVOLibrary(voLibrary);
    }

    // save the vo library
    private static void SaveVOLibrary(List<AudioData> _voLibrary)
    {
        // create a new empty string to store our data
        string audioData = "";

        // for each audio data in the vo library
        foreach (AudioData audioClip in _voLibrary)
        {
            // assign it's json value to the string
            audioData += "[[";
            audioData += JsonUtility.ToJson(audioClip);
            audioData += "]]";
        }

        // get our vo library data
        TextAsset voLibraryData = Resources.Load("voLibraryData") as TextAsset;

        // create a stream writer
        StreamWriter writer;

        // check if our library data file does not exists
        if (voLibraryData == null)
        {
            // check if we have a resource folder
            if (!Directory.Exists(Application.dataPath + "/Resources"))
            {
                // if not create one
                Directory.CreateDirectory(Application.dataPath + "/Resources");
            }

            // create our file if it doesn't exist and refresh the asset database
            writer = new StreamWriter(Application.dataPath + @"/Resources/voLibraryData.json");
        }
        else
        {
            // else if the file exists
            writer = new StreamWriter(AssetDatabase.GetAssetPath(voLibraryData));
        }

        // write our audio data into our audio library data files
        // and refresh the asset database
        writer.Write(audioData);
        writer.Close();
        UnityEditor.AssetDatabase.Refresh();
    }

    // confirm if all the selected subtitle should be removed
    private static void RemoveSelectedSubtitlePopup()
    {
        // get existing open window or if none, make a new one
        removeSelectedPopup = (RemoveSelectedPopup)EditorWindow.GetWindow(typeof(RemoveSelectedPopup), true, "Remove All Selected Subtitles?");
        removeSelectedPopup.maxSize = new Vector2(500f, 90f);
        removeSelectedPopup.minSize = removeSelectedPopup.maxSize;
        EditorExtensions.GetWindowAndCenterOnMain(removeSelectedPopup);
        removeSelectedPopup.Show();
    }

    // remove all the selected subtitle
    public static void RemoveAllSelectedSubtitle()
    {
        // clone the current audio set subtitle list
        List<SubtitleData> currentSubtitleSet = new List<SubtitleData>(audioData.subtitle);

        // remove all subtitle properties that are selected
        currentSubtitleSet.RemoveAll(t => t.selected == true);

        // save the changes 
        audioData.subtitle = currentSubtitleSet;
        LoadSubtitleToVOLibrary();
    }

    // if the window is closed
    private void OnDestroy()
    {
        // load the changes to the vo library
        LoadSubtitleToVOLibrary();

        if (audioData.path != "" && audioData.path != null)
            // uncheck all select boxes
            foreach (SubtitleData data in audioData.subtitle)
            {
                if (data.selected)
                    data.selected = false;
            }
    }

    // if the window falls out of focus
    private void OnLostFocus()
    {
        // load any changes to the library to be safe
        LoadSubtitleToVOLibrary();
    }

    // if the window falls into focus
    private void OnFocus()
    {
        // load any changes to the library to be safe
        LoadSubtitleToVOLibrary();
    }

    // draw the ui 
    void OnGUI()
    {
        /// GUI STYLES

        // editor window styles
        GUIStyle styleWindow = new GUIStyle();
        styleWindow.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(69, 72, 77, 1));

        //subtitle list option styles
        GUIStyle styleSubtitleListOptions = new GUIStyle();
        styleSubtitleListOptions.margin = new RectOffset(25, 25, 25, 8);
        GUIStyle styleClearButton = new GUIStyle(GUI.skin.button);
        styleClearButton.margin = new RectOffset(0, 0, 1, 0);
        styleClearButton.fontSize = 9;
        GUIStyle styleSoundBankListOptions = new GUIStyle();
        styleSoundBankListOptions.margin = new RectOffset(25, 25, 5, 25);

        // subtitle list box styles
        GUIStyle styleSubtitleText = new GUIStyle();
        styleSubtitleText.padding = new RectOffset(6, 6, 6, 6);
        styleSubtitleText.normal.textColor = EditorStyle.ConvertToColor(255, 255, 255, 1);
        GUIStyle styleSubtitleListBox = new GUIStyle();
        styleSubtitleListBox.margin = new RectOffset(25, 25, 0, 0);
        styleSubtitleListBox.padding = new RectOffset(1, 1, 1, 1);
        styleSubtitleListBox.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(85, 85, 85, 1));
        GUIStyle styleSubtitleListHeaderBorder = new GUIStyle();
        styleSubtitleListHeaderBorder.padding = new RectOffset(0, 0, 1, 1);
        styleSubtitleListHeaderBorder.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(85, 85, 85, 1));
        GUIStyle styleSubtitleListHeader = new GUIStyle();
        styleSubtitleListHeader.padding = new RectOffset(0, 0, 4, 2);
        styleSubtitleListHeader.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(108, 111, 116, 1));
        GUIStyle styleSubtitleListHeaderLabel = new GUIStyle();
        styleSubtitleListHeaderLabel.normal.textColor = EditorStyle.ConvertToColor(219, 219, 219, 1);
        styleSubtitleListHeaderLabel.padding = new RectOffset(6, 20, 0, 0);
        GUIStyle styleSubtitleListContainer = new GUIStyle();
        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleSubtitleListContainer.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(96, 99, 104, 1));
        else
            styleSubtitleListContainer.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(140, 140, 140, 1));

        /// EDITOR

        // begin the scroll view
        if (EditorPrefs.GetInt("UserSkin") == 1)
            m_vecScrollPos = EditorGUILayout.BeginScrollView(m_vecScrollPos, styleWindow);
        else
            m_vecScrollPos = EditorGUILayout.BeginScrollView(m_vecScrollPos);
        EditorGUILayout.BeginVertical();

        // subtitle list box options
        EditorGUILayout.BeginHorizontal(styleSubtitleListOptions);
        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("ID: " + audioData.id);
        EditorGUILayout.LabelField("Length: " + audioData.length.ToString("F2") + "s");
        if (audioData.fileName == "" || audioData.fileName == null)
            EditorGUILayout.LabelField("File: -");
        else
            EditorGUILayout.LabelField("File: " + audioData.fileName + audioData.extension);
        if (audioData.path == "" || audioData.path == null)
            EditorGUILayout.LabelField("Path: -");
        else
            EditorGUILayout.LabelField("Path: " + audioData.path);

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Unique Name:", GUILayout.MinWidth(90.0f), GUILayout.MaxWidth(90.0f));
        audioData.uniqueName = EditorGUILayout.TextField(audioData.uniqueName);

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();

        // subtitle list header
        EditorGUILayout.BeginVertical(styleSubtitleListBox);
        m_vecScrollPosSubtitleHeaderLabel = EditorGUILayout.BeginScrollView(new Vector3(m_vecScrollPosSubtitleList.x, m_vecScrollPosSubtitleHeaderLabel.y), GUIStyle.none, GUIStyle.none, GUILayout.MinHeight(23.0f), GUILayout.MaxHeight(23.0f));
        EditorGUILayout.BeginHorizontal(styleSubtitleListHeader);

        EditorGUILayout.LabelField("Select", styleSubtitleListHeaderLabel, GUILayout.MinWidth(50.0f), GUILayout.MaxWidth(50.0f));
        EditorGUILayout.LabelField("Duration", styleSubtitleListHeaderLabel, GUILayout.MinWidth(70.0f), GUILayout.MaxWidth(70.0f));
        EditorGUILayout.LabelField("Subtitle", styleSubtitleListHeaderLabel, GUILayout.MinWidth(325.0f));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal(styleSubtitleListHeaderBorder);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndScrollView();

        GUIStyle styleIncludeToggle = new GUIStyle();
        styleIncludeToggle.padding = new RectOffset(22, 32, 0, 0);
        styleIncludeToggle.margin = new RectOffset(0, 0, 1, 0);
        GUIStyle styleSubtitleInputFields = new GUIStyle(GUI.skin.textField);
        styleSubtitleInputFields.margin = new RectOffset(0, 0, 5, 0);

        // subtitle listing begins here
        m_vecScrollPosSubtitleList = EditorGUILayout.BeginScrollView(m_vecScrollPosSubtitleList, styleSubtitleListContainer, GUILayout.MinHeight(160.0f), GUILayout.MaxHeight(Screen.height));

        // reset the index count
        index = 0;

        // if the file name is not null
        if (audioData.fileName == null)
            EditorGUILayout.LabelField("To edit a clip's subtitle, you must first select it in the Audio Library.", styleSubtitleText);
        else
        {
            // if there subtitle count does not equal 0
            if (audioData.subtitle.Count != 0)
            {
                // display each subtitle and its properties
                foreach (SubtitleData subtitle in audioData.subtitle)
                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.BeginHorizontal(styleIncludeToggle, GUILayout.MinWidth(50.0f), GUILayout.MaxWidth(50.0f));
                    subtitle.selected = EditorGUILayout.Toggle(subtitle.selected);
                    EditorGUILayout.EndHorizontal();

                    subtitle.duration = EditorGUILayout.FloatField(subtitle.duration, styleSubtitleInputFields, GUILayout.MinWidth(50.0f), GUILayout.MaxWidth(50.0f));
                    EditorGUILayout.LabelField("", GUILayout.MinWidth(15.0f), GUILayout.MaxWidth(15.0f));
                    subtitle.subtitle = EditorGUILayout.TextField(subtitle.subtitle, styleSubtitleInputFields, GUILayout.MinWidth(325.0f));

                    // if the remove button is pressed
                    if (GUILayout.Button("Remove", GUILayout.MinWidth(75.0f), GUILayout.MaxWidth(75.0f)))
                    {
                        // clone the current audio set subtitle list
                        List<SubtitleData> currentSubtitleSet = new List<SubtitleData>(audioData.subtitle);

                        // removed that current subtitle
                        currentSubtitleSet.RemoveAt(index);

                        // save the changes 
                        audioData.subtitle = currentSubtitleSet;
                        LoadSubtitleToVOLibrary();
                    }

                    EditorGUILayout.LabelField("", GUILayout.MinWidth(15.0f), GUILayout.MaxWidth(15.0f));

                    EditorGUILayout.EndHorizontal();

                    // increase the row count
                    index++;
                }
            }
            else
            {
                // else display our labels
                EditorGUILayout.LabelField("There is no subtitle for this clip.", styleSubtitleText);
                EditorGUILayout.LabelField("To add subtitle press the Add Subtitle button below.", styleSubtitleText);
            }
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
        EditorGUILayout.BeginHorizontal(styleSoundBankListOptions);

        // add another subtitle property to the list if this button is pressed
        if (GUILayout.Button("Add Subtitle", GUILayout.MinWidth(100.0f), GUILayout.MaxWidth(100.0f)))
        {
            if (audioData.path != "" && audioData.path != null)
            {
                SubtitleData newSubtitle = new SubtitleData();
                newSubtitle.subtitle = "";
                newSubtitle.duration = 0.0f;
                newSubtitle.selected = false;
                audioData.subtitle.Add(newSubtitle);
                LoadSubtitleToVOLibrary();
            }
        }

        // select each subtitle property in the list
        if (GUILayout.Button("Select All", GUILayout.MinWidth(90.0f), GUILayout.MaxWidth(90.0f)))
        {
            if (audioData.path != "" && audioData.path != null)
            {
                foreach (SubtitleData subtitle in audioData.subtitle)
                    subtitle.selected = true;
            }
        }

        // deselect each subtitle property in the list
        if (GUILayout.Button("Deselect All", GUILayout.MinWidth(90.0f), GUILayout.MaxWidth(90.0f)))
        {
            if (audioData.path != "" && audioData.path != null)
            {
                foreach (SubtitleData subtitle in audioData.subtitle)
                    subtitle.selected = false;
            }
        }

        // remove all selected subtitle properties in the list
        if (GUILayout.Button("Remove Selected", GUILayout.MinWidth(130.0f), GUILayout.MaxWidth(130.0f)))
        {
            if (audioData.path != "" && audioData.path != null)
            {
                RemoveSelectedSubtitlePopup();
            }
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();

        // detect if any gui values has changed
        /*if (GUI.changed)
        {
            if (audioData.path != "" && audioData.path != null)
                // load the changes to the vo library
                LoadSubtitleToVOLibrary();
        }*/
    }
}

// confirmation window to remove all subtitle entries from the current clip in the vo library
public class RemoveSelectedPopup : EditorWindow
{
    // draw our ui
    void OnGUI()
    {
        /// GUI STYLES

        GUIStyle styleText = new GUIStyle();
        styleText.padding = new RectOffset(10, 10, 10, 10);
        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleText.normal.textColor = EditorStyle.ConvertToColor(168, 168, 168, 1);
        styleText.alignment = TextAnchor.MiddleLeft;
        styleText.wordWrap = true;
        styleText.fontSize = 12;

        /// EDITOR

        EditorGUILayout.LabelField("Are you sure you want to remove the selected subtitles? Please keep in mind this change cannot be undone.", styleText);

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        // exclude all selected files from the vo library if this button is pressed
        if (GUILayout.Button("Proceed", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
        {
            SubtitleEditor.RemoveAllSelectedSubtitle();
            this.Close();
        }

        // close this window if this button is pressed
        if (GUILayout.Button("Cancel", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
            this.Close();

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }
}