﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// VO Library - GUI
/// Description: Contains a list and reference to all audio files throughout the Unity project.
/// </summary>

// confirmation window to name and generate a VO Bank into the prefabs folder
public class GenerateVOBankPopup : EditorWindow
{
    // prefab settings
    private string prefabName = "VOBank";

    // add "Clear Data" option under the VO Manager context menu
    [MenuItem("VO Manager/Generate VO Bank")]

    // on initialization
    static void Init()
    {
        // get existing open window or if none, make a new one
        GenerateVOBankPopup window = (GenerateVOBankPopup)EditorWindow.GetWindow(typeof(GenerateVOBankPopup), true, "Generate VO Bank");
        window.maxSize = new Vector2(500f, 110f);
        window.minSize = window.maxSize;
        EditorExtensions.GetWindowAndCenterOnMain(window);
        window.Show();
    }

    // draw the ui
    void OnGUI()
    {
        /// GUI STYLES

        GUIStyle styleText = new GUIStyle();
        styleText.padding = new RectOffset(10, 10, 10, 10);
        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleText.normal.textColor = EditorStyle.ConvertToColor(168, 168, 168, 1);
        styleText.alignment = TextAnchor.MiddleLeft;
        styleText.wordWrap = true;
        styleText.fontSize = 12;
        GUIStyle styleInput = new GUIStyle();
        styleInput.padding = new RectOffset(10, 10, 0, 0);
        styleInput.margin = new RectOffset(0, 0, 0, 10);

        /// EDITOR

        EditorGUILayout.LabelField("Please give your prefab a name. All VO Banks by default will be generated into the Prefabs folder.", styleText);

        EditorGUILayout.BeginHorizontal(styleInput);

        prefabName = EditorGUILayout.TextField(prefabName);

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        // generate the vo bank if the button is pressed
        if (GUILayout.Button("Save", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
        {
            // get existing open window or if none, make a new one
            VOLibrary window = (VOLibrary)EditorWindow.GetWindow(typeof(VOLibrary), false, "VO Library");
            window.Show();
            window.GenerateVOBank(prefabName);

            // close this window
            this.Close();
        }

        // close the current window if the button is pressed
        if (GUILayout.Button("Cancel", GUILayout.MinWidth(105.0f), GUILayout.MaxWidth(105.0f)))
            this.Close();

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }
}