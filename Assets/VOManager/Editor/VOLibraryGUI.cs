﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

/// <summary>
/// VO Library - GUI
/// Description: Contains a list and reference to all audio files throughout the Unity project.
/// </summary>

public partial class VOLibrary : EditorWindow
{
    // print the current row for the audio data
    private void PrintRowInfo(AudioData audioClip)
    {
        /// GUI STYLES

        RectOffset rowPadding = new RectOffset(0, 0, 2, 4);
        RectOffset rowTextPadding = new RectOffset(0, 0, 4, 0);
        Color rowTextColor = new Color();

        if (EditorPrefs.GetInt("UserSkin") == 1)
            rowTextColor = EditorStyle.ConvertToColor(168, 168, 168, 1);
        else
            rowTextColor = EditorStyle.ConvertToColor(219, 219, 219, 1);

        // audio field styles
        GUIStyle styleOddRow = new GUIStyle();
        styleOddRow.normal.background = EditorStyle.SetBackground(1, 1, new Color(0.1f, 0.1f, 0.1f, 0.2f));
        styleOddRow.padding = rowPadding;
        GUIStyle styleEvenRow = new GUIStyle();
        styleEvenRow.padding = rowPadding;
        GUIStyle styleRowText = new GUIStyle();
        styleRowText.padding = new RectOffset(10, 0, 4, 0);
        styleRowText.normal.textColor = rowTextColor;
        styleRowText.alignment = TextAnchor.MiddleLeft;
        GUIStyle styleRowCenterText = new GUIStyle();
        styleRowCenterText.padding = rowTextPadding;
        styleRowCenterText.normal.textColor = rowTextColor;
        styleRowCenterText.alignment = TextAnchor.MiddleCenter;
        GUIStyle styleIncludeToggle = new GUIStyle();
        styleIncludeToggle.padding = new RectOffset(25, 25, 0, 0);

        // missing clip styles
        GUIStyle styleMissingClipEven = new GUIStyle();
        styleMissingClipEven.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(200, 40, 40, 1));
        styleMissingClipEven.padding = rowPadding;
        GUIStyle styleMissingClipOdd = new GUIStyle();
        styleMissingClipOdd.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(140, 20, 20, 1));
        styleMissingClipOdd.padding = rowPadding;

        /// EDITOR

        // for each even row
        if (row % 2 != 0)
            // if the audio clip is missing
            if (audioClip.isMissing)
                EditorGUILayout.BeginHorizontal(styleMissingClipEven);
            else
                EditorGUILayout.BeginHorizontal(styleOddRow);
        else
            // if the audio clip is missing
            if (audioClip.isMissing)
            EditorGUILayout.BeginHorizontal(styleMissingClipOdd);
        else
            EditorGUILayout.BeginHorizontal(styleEvenRow);

        // if the edit button is pressed
        if (GUILayout.Button("Edit", GUILayout.MinWidth(55.0f), GUILayout.MaxWidth(55.0f)))
        {
            SubtitleEditor window = (SubtitleEditor)EditorWindow.GetWindow(typeof(SubtitleEditor), true, "Subtitle Editor");
            window.EndWindows();
            window.SetAudioData(audioClip);
            window.maxSize = new Vector2(750f, 600f);
            window.minSize = window.maxSize;
            EditorExtensions.GetWindowAndCenterOnMain(window);
            window.Show();
        }

        EditorGUILayout.LabelField(audioClip.id.ToString(), styleRowCenterText, GUILayout.MinWidth(30.0f), GUILayout.MaxWidth(30.0f));
        EditorGUILayout.LabelField(audioClip.length.ToString("F2") + "s", styleRowCenterText, GUILayout.MinWidth(60.0f), GUILayout.MaxWidth(60.0f));
        EditorGUILayout.LabelField(audioClip.uniqueName, styleRowText, GUILayout.MinWidth(150.0f), GUILayout.MaxWidth(150.0f));
        EditorGUILayout.LabelField(audioClip.filePath, styleRowText, GUILayout.MinWidth(150.0f), GUILayout.MaxWidth(250.0f));

        EditorGUILayout.EndHorizontal();
    }

    // draw the ui
    void OnGUI()
    {
        /// GUI STYLES

        RectOffset rowHeaderTextPadding = new RectOffset(0, 0, 0, 0);
        Color rowHeaderTextColor = EditorStyle.ConvertToColor(219, 219, 219, 1);

        // editor window styles
        GUIStyle styleWindow = new GUIStyle();
        styleWindow.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(69, 72, 77, 1));

        // audio list option styles
        GUIStyle styleAudioListOptions = new GUIStyle();
        styleAudioListOptions.margin = new RectOffset(25, 25, 25, 8);
        GUIStyle styleClearButton = new GUIStyle(GUI.skin.button);
        styleClearButton.margin = new RectOffset(0, 0, 1, 0);
        styleClearButton.fontSize = 9;
        GUIStyle styleFooterListOptions = new GUIStyle();
        styleFooterListOptions.margin = new RectOffset(25, 25, 5, 25);

        // audio list box styles
        GUIStyle styleAudioListBox = new GUIStyle();
        styleAudioListBox.margin = new RectOffset(25, 25, 0, 0);
        styleAudioListBox.padding = new RectOffset(1, 1, 1, 1);
        styleAudioListBox.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(85, 85, 85, 1));
        GUIStyle styleAudioListHeaderBorder = new GUIStyle();
        styleAudioListHeaderBorder.padding = new RectOffset(0, 0, 1, 1);
        GUIStyle styleAudioListContainer = new GUIStyle();

        if (EditorPrefs.GetInt("UserSkin") == 1)
            styleAudioListContainer.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(96, 99, 104, 1));
        else
            styleAudioListContainer.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(140, 140, 140, 1));

        // audio list header styles
        GUIStyle styleRowHeader = new GUIStyle();
        styleRowHeader.padding = new RectOffset(0, 0, 3, 3);
        styleRowHeader.normal.background = EditorStyle.SetBackground(1, 1, EditorStyle.ConvertToColor(108, 111, 116, 1));
        GUIStyle styleRowHeaderCenterText = new GUIStyle();
        styleRowHeaderCenterText.padding = rowHeaderTextPadding;
        styleRowHeaderCenterText.normal.textColor = rowHeaderTextColor;
        styleRowHeaderCenterText.alignment = TextAnchor.MiddleCenter;
        GUIStyle styleRowHeaderText = new GUIStyle();
        styleRowHeaderText.padding = new RectOffset(10, 0, 0, 0);
        styleRowHeaderText.normal.textColor = rowHeaderTextColor;
        styleRowHeaderText.alignment = TextAnchor.MiddleLeft;
        
        /// EDITOR

        // begin the scroll view
        if (EditorPrefs.GetInt("UserSkin") == 1) 
            m_vecScrollPos = EditorGUILayout.BeginScrollView(m_vecScrollPos, styleWindow);
        else
            m_vecScrollPos = EditorGUILayout.BeginScrollView(m_vecScrollPos);
        EditorGUILayout.BeginVertical();

        // audio list box options
        EditorGUILayout.BeginHorizontal(styleAudioListOptions);
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Search", GUILayout.MinWidth(45.0f), GUILayout.MaxWidth(45.0f));
        searchText = EditorGUILayout.TextField("", searchText, GUILayout.MinWidth(175.0f), GUILayout.MaxWidth(175.0f));
        searchBy = (SearchBy)EditorGUILayout.EnumPopup("", searchBy, GUILayout.MinWidth(90.0f), GUILayout.MaxWidth(90.0f));

        // clear the search box if the button is clicked on
        if (GUILayout.Button("Clear", styleClearButton, GUILayout.MinWidth(55.0f), GUILayout.MaxWidth(55.0f)))
        {
            searchText = "";
            GUI.FocusControl("");
        }

        EditorGUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Order By", GUILayout.MinWidth(60.0f), GUILayout.MaxWidth(60.0f));

        orderBy = (OrderBy)EditorGUILayout.EnumPopup("", orderBy, GUILayout.MinWidth(85.0f), GUILayout.MaxWidth(85.0f));
        sortBy = (SortBy)EditorGUILayout.EnumPopup("", sortBy, GUILayout.MinWidth(50.0f), GUILayout.MaxWidth(50.0f));

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndHorizontal();

        // audio clip list
        EditorGUILayout.BeginVertical(styleAudioListBox);
        m_vecScrollPosAudioHeaderLabel = EditorGUILayout.BeginScrollView(new Vector3(m_vecScrollPosAudioList.x, m_vecScrollPosAudioHeaderLabel.y), GUIStyle.none, GUIStyle.none, GUILayout.MinHeight(23.0f), GUILayout.MaxHeight(23.0f));

        EditorGUILayout.BeginHorizontal(styleRowHeader);

        EditorGUILayout.LabelField("", styleRowHeaderCenterText, GUILayout.MinWidth(55.0f), GUILayout.MaxWidth(55.0f));
        EditorGUILayout.LabelField("ID", styleRowHeaderCenterText, GUILayout.MinWidth(30.0f), GUILayout.MaxWidth(30.0f));
        EditorGUILayout.LabelField("Length", styleRowHeaderCenterText, GUILayout.MinWidth(60.0f), GUILayout.MaxWidth(60.0f));
        EditorGUILayout.LabelField("Unique Name", styleRowHeaderText, GUILayout.MinWidth(150.0f), GUILayout.MaxWidth(150.0f));
        EditorGUILayout.LabelField("File Path", styleRowHeaderText, GUILayout.MinWidth(150.0f), GUILayout.MaxWidth(250.0f));

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal(styleAudioListHeaderBorder);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndScrollView();

        // audio clip listing begins here
        m_vecScrollPosAudioList = EditorGUILayout.BeginScrollView(m_vecScrollPosAudioList, styleAudioListContainer, GUILayout.MinHeight(160.0f), GUILayout.MaxHeight(Screen.height));

        // reset the row count
        row = 0;

        // create a copy of our vo library
        List<AudioData> audioList = new List<AudioData>(OrderAndSortList(voLibrary));

        // for each audio clip in the library
        foreach (AudioData audioClip in audioList)
        {
            // if the audio file is in the vo library
            if (audioClip.includeInVOLibrary)
            {
                // if the search box is empty
                if (searchText == null || searchText == "")
                    PrintRowInfo(audioClip);
                else
                {
                    // if the audio clip contains the search word pertaining to the filter
                    if (searchBy == SearchBy.ID)
                    {
                        if (audioClip.id.ToString().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                            PrintRowInfo(audioClip);
                    }
                    else if (searchBy == SearchBy.UniqueName)
                    {
                        if (audioClip.uniqueName.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                            PrintRowInfo(audioClip);
                    }
                    else if (searchBy == SearchBy.FilePath)
                    {
                        if (audioClip.filePath.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                            PrintRowInfo(audioClip);
                    }
                }

                // increase the row count
                row++;
            }
        }

        // if there is no vo library loaded
        if (row == 0)
        {
            GUIStyle styleText = new GUIStyle();
            styleText.padding = new RectOffset(10, 10, 10, 10);
            if (EditorPrefs.GetInt("UserSkin") == 1)
                styleText.normal.textColor = EditorStyle.ConvertToColor(168, 168, 168, 1); 
            else
                styleText.normal.textColor = EditorStyle.ConvertToColor(255, 255, 255, 1);
            styleText.alignment = TextAnchor.MiddleLeft;
            styleText.wordWrap = true;
            styleText.fontSize = 12;
            EditorGUILayout.LabelField("There are no audio files in your VO Library. Please add them from the Audio Library.", styleText);
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
        EditorGUILayout.BeginHorizontal(styleFooterListOptions);
        GUILayout.FlexibleSpace();

        // reload the vo library if the button is clicked on
        if (GUILayout.Button("Reload VO Library", GUILayout.MinWidth(125.0f), GUILayout.MaxWidth(125.0f)))
            LoadVOLibrary();

        // generate a vo library prefab
        if (GUILayout.Button("Generate VO Bank", GUILayout.MinWidth(125.0f), GUILayout.MaxWidth(125.0f)))
            GenerateVOBankPopup();

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();
    }
}
