﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Playables;
using UnityEngine;
using Vuforia;

public class ImageTargetPlayAudioSubtitles : MonoBehaviour,
ITrackableEventHandler
{
	private TrackableBehaviour mTrackableBehaviour;

	public AudioSource audSrcCivic;   // the audio source for the Civic
	public float fltDelayBetweenCivicAudio = 1.0f;  // the delay between audio files
	public bool blnCanTriggerCivicAudio; // checks if the Civic audio can be triggered
	public bool blnCivicAudioIsTriggered;    // check if the Civic audio is triggered

	// an array to store the order of which our audio performs
	private int[] CivicAudio = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	private int m_intCurrentIndex = 0;
	private float m_fltDelayBetweenAudio = 0.6f;
	private bool blnIsAudioPlaying = false;

	void Start()
	{
		blnIsAudioPlaying = false;
		m_fltDelayBetweenAudio = fltDelayBetweenCivicAudio;
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	void Update()
	{
		if (blnIsAudioPlaying)
		{
			// if the current index is less than the list length
			if (m_intCurrentIndex < CivicAudio.Length)
			{
				// if the delay rate between the audio files is 0
				if (m_fltDelayBetweenAudio <= 0)
				{
					// play the audio at the audio source and increment the index
					VOManager.Instance.Play(audSrcCivic, CivicAudio[m_intCurrentIndex]);
					m_intCurrentIndex++;
					Debug.Log("Lecture");

					// reset the delay rate
					m_fltDelayBetweenAudio = fltDelayBetweenCivicAudio;
				}
				else
					// decrease the delay rate based on delta time
					m_fltDelayBetweenAudio -= Time.deltaTime;
			}
			else
			{
				// turn off the audio triggers
				m_intCurrentIndex = 0;
				blnIsAudioPlaying = false;
			}
		}
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
			Debug.Log("Cible traquee");
		{

			blnIsAudioPlaying = true;
		}
	}
}