﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundAndAnim : MonoBehaviour {

	GameObject BPlay, Cube;

	void Start () {
		BPlay = GameObject.Find ("Play");
		Cube = GameObject.Find ("Cube");
			
		BPlay.SetActive (true);
	}

	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				if(hit.collider.tag=="Play")
				{  
					Cube.GetComponent<AudioSource>().Play();
				}
				 
			}
		}
	}
}
				

