﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Vuforia;

public class MainMenu : MonoBehaviour {

	public string gameSceneName;

	public void StartAC()
	{
		SceneManager.LoadScene (1);
		VuforiaRuntime.Instance.InitVuforia ();
	}

	public void StartGN()
	{
		SceneManager.LoadScene (2);
		VuforiaRuntime.Instance.InitVuforia ();
	}

}
