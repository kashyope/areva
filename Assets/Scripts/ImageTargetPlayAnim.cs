﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Playables;
using UnityEngine;
using Vuforia;

public class ImageTargetPlayAnim : MonoBehaviour,
ITrackableEventHandler
{
	private TrackableBehaviour mTrackableBehaviour;
	public Animator LineAnim;

	void Start()
	{
		LineAnim.GetComponent<Animator>();
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			// Play anim when target is found
			LineAnim.Play("all");
			Debug.Log("Line Start");
		}
		else
		{
			// Stop anim when target is lost
			LineAnim.Play("none");
			Debug.Log("Line Stop");
		}
	}   
}