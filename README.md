Photoptimiste is a mobile augmented reality app design by Eva Barbara and crafted by KashYope.
The app uses Vuforia to recognize Eva's pictures and display some digital assets like 2D designs, 3D objects or sounds.
As I'm still learning Unity, all the functions are very basics and could probably be done in a more simple way.
There is all the functionnalities of the app so far :

Delay Vuforia initialization (for faster start)
Simple menu with external links
Autofocus script for old mobile phone
Touch script to create interactive objects
Auto play sound script when a target is recognize

What needs to be implemented :

AWS support for extra content
Screenshot and camera roll saving function
Sync subtitles when specific target is recognize